package com.bibao.boot.repository;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.entity.EmployeeEntity;
import com.bibao.boot.springbooth2.SpringBootH2Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootH2Application.class)
public class EmployeeRepositoryIntegTest {
	@Autowired
	private EmployeeRepository empRepo;
	
	@Test
	public void testFindByName() {
		EmployeeEntity entity = empRepo.findByName("Alice");
		assertNotNull(entity);
		assertEquals("Alice", entity.getName());
		assertEquals(57200, entity.getSalary());
		assertEquals("1985-05-03", entity.getBirthday().toString());
	}
	
	@Test
	public void testFindByBirthday() {
		LocalDate startDate = LocalDate.of(1985, 1, 1);
		LocalDate endDate = LocalDate.of(1990, 1, 1);
		List<EmployeeEntity> entities = empRepo.findByBirthday(startDate, endDate);
		assertEquals(3, entities.size());
	}

	@Test
	public void testUpdateSalaryByName() {
		empRepo.updateSalaryByName("Bob", 1000);
		EmployeeEntity entity = empRepo.findByName("Bob");
		assertEquals(86300, entity.getSalary());
	}
	
}
