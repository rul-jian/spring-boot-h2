package com.bibao.boot.repository;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.entity.EmployeeEntity;
import com.bibao.boot.springbooth2.SpringBootH2Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootH2Application.class)
@TestPropertySource("classpath:application-h2.properties")
public class EmployeeRepositoryTest {
	@Autowired
	private EmployeeRepository empRepo;
	
	@Before
	public void setUp() throws Exception {
		List<EmployeeEntity> entities = new ArrayList<>();
		entities.add(createInstance("Alice", 72000, 1985, 4, 12));
		entities.add(createInstance("Bob", 58000, 1987, 8, 25));
		entities.add(createInstance("Steven", 49000, 1992, 2, 18));
		entities.add(createInstance("Jason", 64000, 1984, 9, 7));
		empRepo.saveAll(entities);
	}

	@After
	public void tearDown() throws Exception {
		empRepo.deleteAll();
	}

	@Test
	public void testFindByName() {
		EmployeeEntity entity = empRepo.findByName("Alice");
		assertNotNull(entity);
		assertEquals("Alice", entity.getName());
		assertEquals(72000, entity.getSalary());
		assertEquals("1985-04-12", entity.getBirthday().toString());
	}
	
	@Test
	public void testFindByBirthday() {
		LocalDate startDate = LocalDate.of(1985, 1, 1);
		LocalDate endDate = LocalDate.of(1990, 1, 1);
		List<EmployeeEntity> entities = empRepo.findByBirthday(startDate, endDate);
		assertEquals(2, entities.size());
	}

	@Test
	public void testUpdateSalaryByName() {
		empRepo.updateSalaryByName("Bob", 1000);
		EmployeeEntity entity = empRepo.findByName("Bob");
		assertEquals(59000, entity.getSalary());
	}
	
	private EmployeeEntity createInstance(String name, int salary, int year, int month, int day) {
		EmployeeEntity entity = new EmployeeEntity();
		entity.setName(name);
		entity.setSalary(salary);
		entity.setBirthday(LocalDate.of(year, month, day));
		return entity;
	}
}
