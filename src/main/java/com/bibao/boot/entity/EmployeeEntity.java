package com.bibao.boot.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="employee")
public class EmployeeEntity implements Serializable {	
	private static final long serialVersionUID = 7556981277668290835L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emp_id_generator")
	@SequenceGenerator(name="emp_id_generator", sequenceName = "employee_seq", allocationSize=1)
    @Column
	private int id;
	@Column
	private String name;
	@Column
	private int salary;
	@Column
	private LocalDate birthday;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

}
