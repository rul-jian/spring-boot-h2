package com.bibao.boot.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.bibao.boot.entity.EmployeeEntity;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Integer> {
	public EmployeeEntity findByName(String name);
	
	@Query("select e from EmployeeEntity e where e.birthday between :startDate and :endDate")
	public List<EmployeeEntity> findByBirthday(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);
	
	@Modifying
	@Transactional
	@Query("update EmployeeEntity e set e.salary = e.salary + :increasedSalary where e.name = :name")
	public int updateSalaryByName(@Param("name") String name, @Param("increasedSalary") int increasedSalary);

}
